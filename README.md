# Calendar App (calendar)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### For API Url please see src/util/envConfig 

If I still have time I could divide the calendar into smaller components and more organized way

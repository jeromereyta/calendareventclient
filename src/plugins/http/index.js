import axios from 'axios'

/**
 * Create Axios
 */
export const http = axios.create({
    baseURL: '',
})



/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
*/
http.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};


export default function install(Vue) {
    Object.defineProperty(Vue.prototype, '$http', {
        get() {
            return http
        },
    })
}